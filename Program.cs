﻿using System;

namespace MyApplication
{
    abstract class Vehicle  // Base class
    {
        private string brand = "";  
        public abstract void honk();        // abstract method 
        public string brandName()
        {
            return brand;
        }
    }
    public interface Truck  // interface
    {   
        void getSpeed();
    }

    class Bike : Vehicle
    {
        private string brand = "pulsar"; 
        public override void honk()             // body for abstract method
        {
            Console.WriteLine("peep, peep!");
        }
        public void maxspeed()
        {
            Console.WriteLine("max speed is 100 mps");
        }
        public void maxspeed(int n)
        {
            Console.WriteLine("max speed is 100 mps with "+n.ToString()+" passengers!");
        }
        public string brandName()
        {
            return brand;
        }
    }

    class Car : Vehicle  // Derived class
    {
        private string model = "Ford Mustang"; 
        public string modelName()
        {
            return model;
        }
        public override void honk()             // body for abstract method
        {
            Console.WriteLine("Tuut, tuut!");
        }

    }
    class Tanker: Truck
    {
        private static string speed = "50 mps!"; 
        public void getSpeed()
        {
            Console.WriteLine("speed is "+ speed);
        }
    }
    static class Jet {  //static class
    public static string jetType = "fighter";   
 
    public static void details()        //static method
    {
        Console.WriteLine("That jet is: "+ jetType);
    }
    }

  class Program
  { 
    static void Main(string[] args)
    {
        //Create a Car object
        Car myCar = new Car();
        myCar.honk();
        Console.WriteLine(myCar.brandName() + "" + myCar.modelName());

        //Create a bike object
        Bike myBike = new Bike();
        string nameOfBike = myBike.brandName();
        Console.WriteLine("look at that "+  nameOfBike);
        myBike.honk();
        myBike.maxspeed();                  //polymorphism
        myBike.maxspeed(2);

        //create a Tanker object
        Tanker myTanker = new Tanker();
        Console.WriteLine("Here comes Truck!");
        myTanker.getSpeed();

        //No object needed for Jet as it is static
        Console.WriteLine("WOW!" );
        Jet.details();
    }
  }
}
